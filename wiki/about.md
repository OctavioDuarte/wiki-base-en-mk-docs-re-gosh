# ¿Cómo documentar?

## Sistema a grandes rasgos

### Directorios y cuerpos de texto


La Wiki levanta esta estructura de carpetas dentro del director del repo llamado `wiki` automáticamente y organiza los archivos en el índice.
Cada contenido se desarrolla en un archivo Markdown. Lo ideal es tener uno por tópico dentro de su directorio.


### Imágenes


Las imágenes de cada documento van en la carpeta `imágenes` con la misma jerarquía que los cuerpos de texto y cada imagen tiene un nombre compuesto que tiene el mismo nombre inicial que el cuerpo de texto donde se va a enlazar, más una diferenciación significativa.
Para usarlas en la Wiki, se necesita copiarlas a la carpet imágenes y enlazarlas en el repo. El enlace base es `https://gitlab.com/REPO/WIKI/-/raw/main/`, seguido del directorio. 

Así queda una de las imágenes del artículo ejemplo: `https://gitlab.com/REPO/WIKI/-/raw/main/imagenes/TOPICO/ARTICULO/IMAGEN.jpeg`.
